exports.mongodb = {
    secret: 'helloThereThisIsNotASQL',
    database: 'mongodb://localhost:27017/gst',
    plainDb: 'mongodb://localhost:27017/'
};
exports.mongodbPool = {}

exports.configEmails = {
	masterEmail: "bhushan.pawar@primustechsys.com",
	statReceiver: ["bhushan.pawar@primustechsys.com", "ashwini.lingojwar@techsoftsoln.com"],
	supportEmail: "ashwini.lingojwar@techsoftsoln.com",
	initStats: false
}

exports.smtpConfig = {
	pool: true,
    logger: true,
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // use SSL 
    auth: {
        user: 'bhushan.pawar@primustechsys.com',
        pass: 'reset123456',
        displayName: "Bhushan From GST Portal"
    }
};

exports.initializeConfig = {
	serverConfig : true
}