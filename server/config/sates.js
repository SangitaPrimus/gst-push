exports.states = [
    {
        "stateCode": "AP",
        "name": "Andhra Pradesh"
    },
    {
        "stateCode": "AR",
        "name": "Arunachal Pradesh"
    },
    {
        "stateCode": "AS",
        "name": "Assam"
    },
    {
        "stateCode": "BR",
        "name": "Bihar"
    },
    {
        "stateCode": "CG",
        "name": "Chhattisgarh"
    },
    {
        "stateCode": "Chandigarh",
        "name": "Chandigarh"
    },
    {
        "stateCode": "DN",
        "name": "Dadra and Nagar Haveli"
    },
    {
        "stateCode": "DD",
        "name": "Daman and Diu"
    },
    {
        "stateCode": "DL",
        "name": "Delhi"
    },
    {
        "stateCode": "GA",
        "name": "Goa"
    },
    {
        "stateCode": "GJ",
        "name": "Gujarat"
    },
    {
        "stateCode": "HR",
        "name": "Haryana"
    },
    {
        "stateCode": "HP",
        "name": "Himachal Pradesh"
    },
    {
        "stateCode": "JK",
        "name": "Jammu and Kashmir"
    },
    {
        "stateCode": "JH",
        "name": "Jharkhand"
    },
    {
        "stateCode": "KA",
        "name": "Karnataka"
    },
    {
        "stateCode": "KL",
        "name": "Kerala"
    },
    {
        "stateCode": "MP",
        "name": "Madhya Pradesh"
    },
    {
        "stateCode": "MH",
        "name": "Maharashtra"
    },
    {
        "stateCode": "MN",
        "name": "Manipur"
    },
    {
        "stateCode": "ML",
        "name": "Meghalaya"
    },
    {
        "stateCode": "MZ",
        "name": "Mizoram"
    },
    {
        "stateCode": "NL",
        "name": "Nagaland"
    },
    {
        "stateCode": "OR",
        "name": "Orissa"
    },
    {
        "stateCode": "PB",
        "name": "Punjab"
    },
    {
        "stateCode": "PY",
        "name": "Pondicherry"
    },
    {
        "stateCode": "RJ",
        "name": "Rajasthan"
    },
    {
        "stateCode": "SK",
        "name": "Sikkim"
    },
    {
        "stateCode": "TN",
        "name": "Tamil Nadu"
    },
    {
        "stateCode": "TR",
        "name": "Tripura"
    },
    {
        "stateCode": "UP",
        "name": "Uttar Pradesh"
    },
    {
        "stateCode": "UK",
        "name": "Uttarakhand"
    },
    {
        "stateCode": "WB",
        "name": "West Bengal"
    }
]