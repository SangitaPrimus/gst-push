
var groups = {
	allUserRoles : ["su", "support", "managers", "guest", "admin"],
	primusAll : ["su", "support", "managers"],
	externalAll : ["guest", "admin"]
}


var contextAccess = 
	[
		{
		  	path: '/',
		  	method: [['GET']],
		  	role: [["company"]]
		},
		{
		  	path: '/login',
		  	method: [['GET', 'POST']],
		  	role: [["guest", "supplier", "company"]]
		},
		
		{
		  	path: '/assets/gst.xlsx',
		  	method: [['GET']],
		  	role: [["guest", "supplier", "company"]]
		},
		{
		  	path: '/register',
		  	method: [['GET', 'POST']],
		  	role: [["guest", "supplier", "company"]]
		},
		{
			path: '/order',
		  	method: [['GET', 'POST']],
		  	role: [ ["supplier" ]]
		},
		{
			path: '/file-gst',
		  	method: [['GET', 'POST']],
		  	role: [ ["supplier"] ]
		}

	]


exports.menus = 
{

	su : [
		{
		  	name: 'Onboard Clients',
		  	icon: 'ti-panel',
		  	path: '/scp/{{companyName}}/su/create-client'
		  	
		},
		{
			name: "View Clients",
		  	icon: "ti-user",
		  	path: '/scp/{{companyName}}/su/view-clients'
		},
		{
			name: "View Users",
		  	icon: "ti-user",
		  	path: '/scp/{{companyName}}/su/users-list'
		},
		{
			name: "Logout",
		  	icon: "ti-user",
		  	path: '/scp/login'	
		}
	],
	supplier : [
		/*{
		  	name: 'Invoice manual entry',
		  	icon: 'ti-panel',
		  	path: '/scp/{{companyName}}/supplier/create-gst'
		  	
		},
		{
			name: "Invoice excel upload",
		  	icon: "ti-user",
		  	path: '/scp/{{companyName}}/supplier/file-gst'
		},
		{
		  	name: 'View Filed invoices ',
		  	icon: 'ti-panel',
		  	path: '/scp/{{companyName}}/supplier/view-gst'
		  	
		},*/
		{
		 	name: "User Profile",
		  	icon: "ti-user",
		  	path: '/scp/{{companyName}}/supplier/user'
		},
		{
			name: "Logout",
		  	icon: "ti-user",
		  	path: '/scp/login'	
		}
	],
	admin : [
		{
		  	name: 'Create Supplier',
		  	icon: 'ti-panel',
		  	path: '/scp/{{companyName}}/admin/create-supplier'
		  	
		},
		{
			name: "View Suppliers",
		  	icon: "ti-user",
		  	path: '/scp/{{companyName}}/admin/view-suppliers'
		},
		{
			name: "Create User",
		  	icon: "ti-user",
		  	path: '/scp/{{companyName}}/admin/create-user'
		},
		{
		  	name: 'View Users',
		  	icon: 'ti-panel',
		  	path: '/scp/{{companyName}}/admin/view-users'
		  	
		},
		{
		  	name: 'Invitation Report',
		  	icon: 'ti-panel',
		  	path: '/scp/{{companyName}}/admin/invite-report'
		  	
		},
		{
			name: "Logout",
		  	icon: "ti-user",
		  	path: '/scp/login'	
		}
	],
	buyer : [	
		/*{
		  	name: 'View GSTs',
		  	icon: 'ti-panel',
		  	path: '/scp/{{companyName}}/buyer/view-gst'
		  	
		},*/
		{
		  	name: 'Create Supplier',
		  	icon: 'ti-panel',
		  	path: '/scp/{{companyName}}/admin/create-supplier'
		  	
		},
		{
			name: "View Suppliers",
		  	icon: "ti-user",
		  	path: '/scp/{{companyName}}/buyer/view-suppliers'
		},
		{
			name: "Logout",
		  	icon: "ti-user",
		  	path: '/scp/login'	
		}
	],
	accountant : [
		{
		  	name: 'Create Supplier',
		  	icon: 'ti-panel',
		  	path: '/scp/{{companyName}}/admin/create-supplier'
		  	
		},
		{
		  	name: 'View GSTs',
		  	icon: 'ti-panel',
		  	path: '/scp/{{companyName}}/accountant/view-gst'
		  	
		},
		{
			name: "View Suppliers",
		  	icon: "ti-user",
		  	path: '/scp/{{companyName}}/accountant/view-suppliers'
		},
		{
			name: "Logout",
		  	icon: "ti-user",
		  	path: '/scp/login'	
		}
	]
}

exports.screen = {
	su : {
		msgLine1 : "Suppliers Collaboration Portal",
		msgLine2 : "Manage Clients",
		msgFooter : "Manage, handle clients quickly"
	},
	admin : {
		msgLine1 : "Invite, manage users",
		msgLine2 : "GSTN and Supplier level activities made easy",
		msgFooter : "Manage, handle users quickly"
	},
	buyer : {
		msgLine1 : "Track and mange invoices",
		msgLine2 : "GSTN and Supplier level activities made easy",
		msgFooter : "Manage, handle users quickly"
	},
	accountant : {
		msgLine1 : "Track and mange invoices",
		msgLine2 : "GSTN and Supplier level activities made easy",
		msgFooter : "Manage, handle users quickly"
	},
	supplier : {
		msgLine1 : "GSTN activities made easy",
		msgLine2 : "Collaborate with company in realtime",
		msgFooter : "Manage, handle all activities quickly"	
	}
}
exports.loadUrlAccess = function (access) {
	for (var i = 0; i < contextAccess.length; i++) {
		var tempNode = contextAccess[i];
		access.allow(tempNode);
	}	
}
