var fs = require("fs")
var colors = require('colors');


var logger = require('tracer')
	.colorConsole({
        format : [
                  "{{timestamp}} <{{title}}> ({{file}}:{{line}}) {{message}} ", //default format 
                  {
                	  error : "{{timestamp}} <{{title}}> {{message}} ({{file}}:{{line}})\nCall Stack:\n{{stack}}" // error format 
                  }
        ],
        dateformat : "dd-mm-yyyy HH:MM:ss.L",
        preprocess :  function(data){
            data.title = data.title.toUpperCase();
        },
        transport : function(data) {
            console.log(data.output);
            try{

                var stream = fs.createWriteStream("./access.log", {
                    flags: "a",
                    encoding: "utf8",
                    mode: 0666
                }).write(data.output+"\n");
            }
            catch(e)
            {

            }
        }
	});

exports.logger = logger;

var reqId = 0, catId = {}, urlId = {}; 
var registerReq = function(req, res, next)
{
	req.reqGid = reqId++,
	res.resGid = req.reqGid;
	res.reqGid = req.reqGid;
		
	
}
exports.registerReq;	
exports.log = function(req, res, next)
{
	registerReq(req, res, next)
    if(Object.keys(req.body).length)
	logger.info( req.url ,"REQUEST" , req.reqGid, req.body);
	
	/*logger.trace('hello', 'world');
	logger.debug('hello %s',  'world', 123);
	logger.info('hello %s %d',  'world', 123, {foo:'bar'});
	logger.warn('hello %s %d %j', 'world', 123, {foo:'bar'});
	logger.error('hello %s %d %j', 'world', 123, {foo:'bar'}, [1, 2, 3, 4], Object);*/
}
