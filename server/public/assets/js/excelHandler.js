var X = XLSX;
var XW = {
	/* worker message */
	msg: 'xlsx',
	/* worker scripts */
	rABS: '/assets/js/xlsParser/xlsxworker2.js',
	norABS: '/assets/js/xlsParser/xlsxworker1.js',
	noxfer: '/assets/js/xlsParser/xlsxworker.js'
};
console.log(XW)
var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
if(!rABS) {
	document.getElementsByName("userabs")[0].disabled = true;
	//document.getElementsByName("userabs")[0].checked = false;
}

var use_worker = typeof Worker !== 'undefined';
if(!use_worker) {
	document.getElementsByName("useworker")[0].disabled = true;
	 //document.getElementsByName("useworker")[0].checked = false;
}

var transferable = use_worker;
if(!transferable) {
	document.getElementsByName("xferable")[0].disabled = true;
	//document.getElementsByName("xferable")[0].checked = false;
}

var wtf_mode = false;

function fixdata(data) {
	var o = "", l = 0, w = 10240;
	for(; l<data.byteLength/w; ++l) o+=String.fromCharCode.apply(null,new Uint8Array(data.slice(l*w,l*w+w)));
	o+=String.fromCharCode.apply(null, new Uint8Array(data.slice(l*w)));
	return o;
}

function ab2str(data) {
	var o = "", l = 0, w = 10240;
	for(; l<data.byteLength/w; ++l) o+=String.fromCharCode.apply(null,new Uint16Array(data.slice(l*w,l*w+w)));
	o+=String.fromCharCode.apply(null, new Uint16Array(data.slice(l*w)));
	return o;
}

function s2ab(s) {
	var b = new ArrayBuffer(s.length*2), v = new Uint16Array(b);
	for (var i=0; i != s.length; ++i) v[i] = s.charCodeAt(i);
	return [v, b];
}

function xw_noxfer(data, cb) {
	var worker = new Worker(XW.noxfer);
	worker.onmessage = function(e) {
		switch(e.data.t) {
			case 'ready': break;
			case 'e': console.error(e.data.d); break;
			case XW.msg: cb(JSON.parse(e.data.d)); break;
		}
	};
	var arr = rABS ? data : btoa(fixdata(data));
	worker.postMessage({d:arr,b:rABS});
}

function xw_xfer(data, cb) {
	var worker = new Worker(rABS ? XW.rABS : XW.norABS);
	
	worker.onmessage = function(e) {
		switch(e.data.t) {
			case 'ready': break;
			case 'e': console.error(e.data.d); alert('cannot process the file, probably invalid file format or extension');break;
			default: xx=ab2str(e.data).replace(/\n/g,"\\n").replace(/\r/g,"\\r"); console.log("done"); cb(JSON.parse(xx)); break;
		}
	};
	if(rABS) {
		var val = s2ab(data);
		worker.postMessage(val[1], [val[1]]);
	} else {
		worker.postMessage(data, [data]);
	}
}

function xw(data, cb) {
	transferable =  true;//document.getElementsByName("xferable")[0].checked;
	if(transferable) xw_xfer(data, cb);
	else xw_noxfer(data, cb);
}

function get_radio_value( radioName ) {
	return true;
	var radios = document.getElementsByName( radioName );
	for( var i = 0; i < radios.length; i++ ) {
		/*if( radios[i].checked || radios.length === 1 ) {
			return radios[i].value;
		}*/
	}
}

function to_json(workbook) {
	var result = {};
	workbook.SheetNames.forEach(function(sheetName) {
		var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
		if(roa.length > 0){
			result[sheetName] = roa;
		}
	});
	return result;
}
function displayProcessingState (msg) {
	console.log(msg);
	/*if(state.innerText === undefined) state.textContent = msg
	else state.innerText = msg*/

	
}
var rows = [];
function to_csv(workbook) {
	var result = [];

	if (parentTable == null) {

		setTableId(null, null, null);

	};
	parentTableHeader.empty();
	parentTableContent.empty();
	
	console.log("utilizing  table " + JSON.stringify(parentTable))
	workbook.SheetNames.forEach(function(sheetName) {
		var csv = X.utils.sheet_to_csv(workbook.Sheets[sheetName]);
		if(csv.length > 0)
		{

			result.push("SHEET: " + sheetName);
			result.push("");
			result.push(csv);
			var processable = csv.split("\n").length;
			displayProcessingState('Found ' + processable + ' rows, rendering ..')
			

			//prepare a row of table
			rows = csv.split("\n");
			
			for (var i = 0; i < rows.length; i++) {
				
				if(!commons.isNull(rows[i].replace(/,/g,"")))
				{
					//var element = '<tr><input type="text val="' + rows[i].split('</input><input type="text val="').join("</input><td>") + '</td>'
					if(i == 0)
					{
						var element = '<tr><th>' + rows[i].split(",").join("</th><th>") + '</th>'
						parentTableHeader.append(element)
					}
					else
					{
						var element = '<tr><td>' + rows[i].split(",").join("</td><td>") + '</td>'
						$(parentTable).append(element)
					}
					console.log(element)
				}


			}
		}
	});
	$(parentTable).trigger("finishedLoadingTable")
	return result.join("\n");
}
parentTable = null

function to_formulae(workbook) {
	var result = [];
	workbook.SheetNames.forEach(function(sheetName) {
		var formulae = X.utils.get_formulae(workbook.Sheets[sheetName]);
		if(formulae.length > 0){
			result.push("SHEET: " + sheetName);
			result.push("");
			result.push(formulae.join("\n"));
		}
	});
	return result.join("\n");
}

var tarea = document.getElementById('b64data');
function b64it() {
	if(typeof console !== 'undefined') {console.log("onload", new Date());}
	var wb = X.read(tarea.value, {type: 'base64', WTF:wtf_mode});
	process_wb(wb);
}

function process_wb(wb) {
	var output = "";
	displayProcessingState('Processsing, please wait !')
	switch(get_radio_value("format")) {
		case "json":
			output = JSON.stringify(to_json(wb), 2, 2);
			console.log(output)
			break;
		case "form":
			output = to_formulae(wb);
			break;
		default:
			
			output = to_csv(wb);
			localStorage.setItem("file",JSON.stringify(to_json(wb), 2, 2));
			
	}/*
	if(out.innerText === undefined) out.textContent = output;
	else out.innerText = output;
	if(typeof console !== 'undefined') console.log("output", new Date());*/
}

var selectedFile = null;

function handleFile(e, b) {
  rABS =true;
  use_worker = true;
  var files = e.target.files;
  var f = files[0];
  
  console.log("Hree is a good", f)
    var reader = new FileReader();
    var name = f.name;
    reader.onload = function(e) {
    	console.log("This can happen now")
      if(typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
      var data = e.target.result;
     
      if(use_worker) {
        xw(data, process_wb);
      } else {
        var wb;
        if(rABS) {
          wb = X.read(data, {type: 'binary'});
        } else {
        var arr = fixdata(data);
          wb = X.read(btoa(arr), {type: 'base64'});
        }
        process_wb(wb);
      }
    };
    if(rABS) reader.readAsBinaryString(f);
    else reader.readAsArrayBuffer(f);
  
}
function getTableId () {
	
	return parentTable;
}
parentTableHeader = null;
parentTableContent = null;
function setTableId (table, header, content) {
 	parentTable = table;
 	parentTableHeader = header;
 	parentTableContent = content;

 	
 	console.log(parentTable, parentTableHeader, parentTableContent)
	
	if(commons.isNull(parentTableHeader))
 	{
 		parentTableHeader = $("#dynamic-table-header")	
 	}

 	if(commons.isNull(parentTableContent))
 	{
 		parentTableContent = $("#dynamic-table-contents")	
 	}
 	
 	
 	if (commons.isNull(table)) {

 		parentTable = $("#dynamic-table")
 	};
}
