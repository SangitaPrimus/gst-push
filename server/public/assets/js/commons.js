var commons = {
    accessDenied : function (url) {
        
        swal({  
                title: "Access Denied!",   
                text: "You are not permitted to access this page.",   
                timer: 10000,   
                type: "error",
                confirmButtonText: "Go back to safety !",
                showCancelButton: false,   
                closeOnConfirm: false,   
                showLoaderOnConfirm: true 
            }, function(){   
                setTimeout(function(){     
                    location.href = (url ? url : "/")
            }, 2000); 
        });
    },
    isNull : function (text) {
        if(text == null || 
            text === null || 
            text == "" ||
            text === "" )
        {
            return true
        }
        return false
    },
    errMsg : function (errText) {
        
        swal({  
            title: "That's not right !",   
            text: errText,   
            timer: 10000,   
            type: "error",
            confirmButtonText: "Close !",
            showCancelButton: false,   
            closeOnConfirm: true,   
            showLoaderOnConfirm: false 
        
        });
    },
    msg : function (title, text) {
        
        swal({  
            title: title,   
            text: text,   
            timer: 20000,   
            type: "success",
            confirmButtonText: "Close !",
            showCancelButton: false,   
            closeOnConfirm: true,   
            showLoaderOnConfirm: false 
        
        });
    },
    checkResponse : function(response, redirectUrl){
       /* if(response.authStatus && response.authStatus.isAuth)
        {
            return true
        }
        commons.accessDenied(redirectUrl)*/
        return true
        //return true

    },
    insertIntoJsonObject: function(obj, cascadedKey, val, splitter) 
    {
        if(cascadedKey == "" || cascadedKey == null)
        {
            return obj;
        }
        // this turns the cascadedKeying into an array = 'one.two.three' becomes ['one', 'two', 'three']
        if(null == splitter || splitter == "")
        {
           splitter = "."
        }
        var arr = cascadedKey.split(splitter);

        // this will be our final object
       
        if(obj == null || obj == undefined)
        {
           obj = new Object();
        }

        // this is the current level of the object - in the first iteration we will add the "one" object here
        var curobj = obj;

        var i = 0;
        // we loop until the next-to-last element because we want the last element ("three") to contain an empty cascadedKeying instead of an empty object
        while (i < (arr.length-1)) {
            // add a new level to the object and set the curobj to the new level
            if(curobj[arr[i]] == null)
            {
                curobj[arr[i]] = new Object();
            }
            curobj = curobj[arr[i++]];
        }
        // finally, we append the value to the final object
        if($.isNumeric(val))
        {
            curobj[arr[i]] = Number(val);
        }
        else
        {
            //console.log(val)

            curobj[arr[i]] = val;
        }
        return obj;
    },

    getSvcUrl: function() {
        return window.location.protocol + "//" + window.location.host
    },

    /*checkResponse : function(result, isToShowToast) 
    {   
        failedCon = readObject("NO_CONNECTION")
        if(failedCon > 3)
        {

            return false;
        }
        if(null == isToShowToast || isToShowToast == undefined)
        {
            isToShowToast = true;
        }
        result = getJsonObject(decoder(result))
        if(commons.isNull(result))
        {
            return false;
        }       
        if(!commons.isNull(result['message']))
        {   
            if(isToShowToast)
            {

                try
                {
                    console.log(result['message'], 3000, "rounded");
                }
                catch(e)
                {
                    alert(result["message"])
                }
            }
            return true;
        }
        if(commons.isNull(result['exception']))
        {
            return true;
        }
        if(!commons.isNull(result['exception']))
        {   
            if(isToShowToast)
            {

                try
                {
                    console.log(result['exception'], 5000);
                }
                catch(e)
                {
                    alert(result["exception"])
                }
                
                try
                {
                    if(result["exception"].indexOf("login again") != -1)
                    {
                        location.href = getValueFromConfig("login")
                    }
                }
                catch(e)
                {

                }
            }
            return false;
        }
        return result['isAuthorized'];
    },*/


    getValueFromConfig: function(key)
    {
        return "login.html"
    },
    isNull: function(val)
    {
        if(val == null || val === null || 
           val === 'undefined' || val == 'undefined' || 
           val == "")
        {
            return true;
        }
        else
        {
            return false;
        }
    },
    ajaxRequest: function (requestUrl, requestData, method) {

        if(typeof(requestData) == 'object')
        {
            //requestData.appName = "CREATIVE"
            requestData = JSON.stringify(requestData)
        }
        if(!method)
        {
            method = (requestData ? "POST" : "GET")
        }
        
        //console.log(requestUrl)
        //getFingerPrint()    
        
        /*var key = CryptoJS.lib.WordArray.random(16);
        var iv= CryptoJS.lib.WordArray.random(16);
        var encrypted = CryptoJS.AES.encrypt(requestData, key, { iv: iv });
        var cipherData = iv.toString(CryptoJS.enc.Base64) + ":" + 
                         encrypted.ciphertext.toString() + ":" + 
                         key.toString(CryptoJS.enc.Base64) + ":" + 
                         readObject("fingerPrint");
        cipherData = (btoa(cipherData))

        requestData = cipherData;*/
        var timeoutSecond = 10;
        response = null;
        return $.ajax({
            type: method,
            contentType: 'application/json', 
            dataType: 'json' ,
            url: requestUrl,
            data: requestData,
            cache: false,
            beforeSend : function(){
               /* $.blockUI({
                    message: '<h5>Please wait....!</h5>',
                    fadeIn: 700, 
                    fadeOut: 700,
                    showOverlay: true,     
                    css: {
                    'font-size': '12px',
                    border: 'none', 
                    padding: '5px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff'} 
                });*/
            },
            success: function (response) {
                /*$.unblockUI({
                    
                    css: { cursor: 'pointer'}
                    
                });*/
                try
                {
                    ////console.log(response)
                    if(checkResponse(response))
                    {
                         return response
                    }
                    
                }
                catch (e)
                {
                    //console.log("Unable to understand response")
                   
                }        
            },
            statusCode: {
                404: function() {
                    console.log("Resource not found", 3000);
                },
                500: function() {
                    console.log( "Internal Server Error",3000 );
                }
            },
            timeout: (timeoutSecond * 1000),
            error: function(XMLHttpRequest, textStatus, errorThrown) {

                if (XMLHttpRequest.readyState == 4) {
                    
                    console.log("HTTP Error",3000)    
                }
                else if (XMLHttpRequest.readyState == 0) {
                    console.log("Could not connect to server",3000)    
                    
                    
                }
                else {
                   console.log("Something is wrong",3000) 
                }
                 /*$.unblockUI({
                    css: {cursor: 'pointer'}
                 });*/
            }
        
        })
    },
    showNotif: function(from, align, message, color){
        /*color = Math.floor((Math.random() * 4) + 1);
        console.warn(color)*/

        $.notify({
            
            message: message,

        },{
            type: type[color],
            timer: 1000,
            placement: {
                from: from,
                align: align
            }
        });
    },
    getCamelCase: function (myString) {
        var camelCased = myString.replace(/_([a-z])/g, function (g) { 
            return " " + g[1].toUpperCase(); 
        });
        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
        return capitalizeFirstLetter(camelCased).trim()
    },
    getCompanyName : function (){
        var companyName = $.cookie('email')
        companyName = companyName.split("@")[1].replace(/\./g,"_")
        return companyName
    }
}




