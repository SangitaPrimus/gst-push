var logger    = require('../config/app-log').logger

var gst = require('../data-models/gst')
var sharedFunctions = require("./share-function")
var request         = require("request")

module.exports = function(app){

    app.post("/get-invoice", function (req, res) {
        console.log(req.body)
        res.render("new-invoice", {invoiceData : req.body})
    })

    app.get("/get-invoice", function (req, res) {
        var invoiceData = [ 
            { 
                gstn_no_of_supplier: 'ARGPS9632EMH',
                supplier_name: 'Alchem',
                invoice_no: '20160001',
                invoice_date: '2016-10-10T18:30:00.000Z',
                gstn_no_buyer__bill_to: 'DIJPS9534MH',
                buyer_name__bill_to: 'Gharda Chemicals',
                buyer_state__bill_to: 'Maharashtra',
                buyer_state_code__bill_to: 'MH',
                gstn_no_buyer__ship_to: 'DIJPS9534MH',
                buyer_name__ship_to: 'Gharda Chemicals',
                buyer_state__ship_to: 'Maharashtra',
                buyer_state_code__ship_to: 'MH',
                description_of_goods: 'Acetic Acid',
                hsn_code: '756012',
                quantity: '100',
                unit: 'Lit',
                rate: '100',
                total: '10000',
                discount: '0',
                taxable_value: '10000',
                cgst_percent: '9',
                cgst_amount: '900',
                sgst_percent: '9',
                sgst_amount: '900',
                igst_percent: '0',
                igst_amount: '0',
                total_invoice_value: '11800' },
              { gstn_no_of_supplier: 'ARGPS9632EMH',
                supplier_name: 'Alchem',
                invoice_no: '20160002',
                invoice_date: null,
                gstn_no_buyer__bill_to: 'DIJPS9534MH',
                buyer_name__bill_to: 'Gharda Chemicals',
                buyer_state__bill_to: 'Maharashtra',
                buyer_state_code__bill_to: 'MH',
                gstn_no_buyer__ship_to: 'SDHIA1235JK',
                buyer_name__ship_to: 'Gharda Chemicals',
                buyer_state__ship_to: 'Jammu Kashmir',
                buyer_state_code__ship_to: 'JK',
                description_of_goods: 'Soda',
                hsn_code: '456735',
                quantity: '100',
                unit: 'KG',
                rate: '50',
                total: '5000',
                discount: '0',
                taxable_value: '5000',
                cgst_percent: '0',
                cgst_amount: '0',
                sgst_percent: '0',
                sgst_amount: '0',
                igst_percent: '18',
                igst_amount: '900',
                total_invoice_value: '5900' } 
            ]
        var sum  = 0
        for (var i = invoiceData.length - 1; i >= 0; i--) {
            sum += Number(invoiceData[i].total_invoice_value)
            console.log(sum)
            if(i == 0)
            {
                res.render("new-invoice", {
                    invoiceData : invoiceData,
                    sum  : sum

                })
            }
        }
    })

    app.get("/scp/:companyName/supplier/order", function (req, res) {
        
        gst.find({}, function (gstErr, gstData) {

            mongoData = sharedFunctions.getMongoData(gstErr, gstData)
            res.render(
                'order', 
                { 
                    title : "Manual Invoice / ASN Filing",
                    gstSchema : gst.schema.paths,
                    orderData : mongoData.data
                }
            )
        });

    })

    
    function unique(array){
        return array.filter(function(el, index, arr) {
            return index === arr.indexOf(el);
        });
    }


}