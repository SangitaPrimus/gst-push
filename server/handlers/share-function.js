var accessLoader  = require("../config/access")

exports.getMongoData = function(dataMongo) {
    return getMongoData(null, dataMongo)
}

exports.getMongoData = function(errorMongo, dataMongo) {

    var readable = []
    //console.log(errorMongo, dataMongo)
    var keys = [], tempKeySet = [], capKeys = []
    if (errorMongo) return console.error(errorMongo);
    else
    {
        for (var i = 0; i < dataMongo.length; i++) {
            var one = JSON.parse(JSON.stringify(dataMongo[i]))
            readable.push(one)

        }
        if(null != readable && null != readable[0])
            var tempKeySet = Object.keys(readable[0])
    } 
    for (var i = tempKeySet.length - 1; i >= 0; i--) {
        keys[i] = tempKeySet[i].replace(/([A-Z]+)/g, " $1").replace(/([A-Z][a-z])/g, " $1")
        
        capKeys[i] = getCamelCase(keys[i])
    }
    //console.log(keys)
    return {data : readable, keys : tempKeySet, capKeys : capKeys}
}

var getCamelCase = function (myString) {
    var camelCased = myString.replace(/_([a-z])/g, function (g) { 
        return " " + g[1].toUpperCase(); 
    });
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    return capitalizeFirstLetter(camelCased).replace(/_/g,"").trim()
}

exports.getCamelCase = getCamelCase