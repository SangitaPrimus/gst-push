	var mongoose        = require('mongoose');
	var config          = require('../config/config'); // get our config file
	var logger          = require('../config/app-log').logger;
	var sharedFunctions = require("../handlers/share-function") // get our config file
	var register 		= require("../data-models/register")
	var emp 			= require('../data-models/emp')
	var gst 			= require('../data-models/gst')
	var admin 			=require("../data-models/company-admin")
	var primusAdmin     = require('../data-models/primus-admin-data-model')
	var fs              = require('fs')
	var path            = require('path')
	var supplierProfile = require("../data-models/user-profile")
	var inviteReport    = require("../data-models/invitation-report")
	
	module.exports = function (app) {
		
		//app.use(contextService.middleware('request'));
		function getDbSwitch(req, res, next) {
			var companyName = 
				//req.params.companyName || 
				(req.cookies.email ? req.cookies.email.split("@")[1] : null)
			if(companyName)
			{
				companyName = companyName.replace(/\./g,"_").replace(":", "")
			
			}
			
			req.switch = {
				companyName : companyName, 
				__uname: req.cookies.email,
				db : "scp__" + companyName,
				role : req.cookies.role || req.params.role
			}
		    console.log(req.switch)
		    req.body.__db = req.switch.db
		}
		
	/*	app.get("/scp/:companyName/*", function (req, res, next) {
			getDbSwitch(req, next)
			
			if(!req.switch.companyName)
			{
				res.redirect(200, "/login?redirectTo=" + req.path)
			}
			//res.json( req.switch )
			next()

		})
	*/	
		var access = require("../config/access")
		var menus = access.menus
		var screen = access.screen
		app.get("/scp/:companyName/:role", function (req, res, next) {
			getDbSwitch(req, next)
			//console.log(app.locals.companyName)
			if(req.cookies.role)
			{
				app.locals.landingMenu = menus[req.cookies.role]
				var oneScreen = screen[req.cookies.role]
				for(var key in oneScreen)
				{
					app.locals[key] = oneScreen[key]
				}
			}

			for(var key in app.locals.landingMenu)
			{
				app.locals.landingMenu[key].path = 
					app.locals.landingMenu[key].path.replace(
						"{{companyName}}", app.locals.companyString
					)
			}
			if(req.switch && req.switch.companyName)
			{

				primusAdmin.find({domain : req.switch.companyName.replace(/_/g,".")}, function (contentE, contentD) {
					if(contentD.length)
					{
						contentD = JSON.parse(JSON.stringify(contentD[0]))
					}
					console.log(contentD.iconUrl	)

					res.render('dashboard', {
						iconUrl : contentD.iconUrl || "/../../../assets/landing/images/pro/logo_e.png" //default logo
					})
				})
			}
			else
			{
					res.render('dashboard', {
						iconUrl : "/../../../assets/landing/images/pro/logo_e.png" //default logo
					})
			}
		})
		app.get("/scp", function (req, res) {
			app.locals.companyName = "Enterprise Garden"
		
			if(req.switch && req.switch.companyName)
			{

				primusAdmin.find({domain : req.switch.companyName.replace(/_/g,".")}, function (contentE, contentD) {
					if(contentD.length)
					{
						contentD = JSON.parse(JSON.stringify(contentD[0]))
					}
					console.log(contentD.iconUrl	)

					res.render('dashboard', {
						iconUrl : contentD.iconUrl || "/../../../assets/landing/images/pro/logo_e.png", //default logo
						isLogin : true
					})
				})
			}
			else
			{
					res.render('dashboard', {
						iconUrl : "/../../../assets/landing/images/pro/logo_e.png", //default logo
						isLogin : true
					})
			}
		})
		app.get("/scp/:companyName/:role/*", function (req, res, next) {
			getDbSwitch(req, next)
			next()
			//res.render('dashboard', {})

		})

		app.post("/scp/:companyName/*", function (req, res, next) {
			getDbSwitch(req, next)
			
			if(!req.switch.companyName)
			{
				res.redirect(200, "/login?redirectTo=" + req.path)
			}
			//res.json( req.switch )
			next()

		})
		
		

		app.get("/scp/:companyName/su", function (req, res) {

			if(req.params.role != "su")
			{
				var filter = { "__db" : req.switch.db }
			}
		    filter.email = req.body.email
		    console.log(filter)
		    register.findOne(filter, function (reErr, reData) {
	    		var r = { data: reData, err: reErr}
	    		r.ok = (reErr ? false : true)
		    	if(reData)
		    	{
		    		//console.log("email this user to " + req.body.toEmail)
		    		require("../handlers/email-handler").sendEmail(req.body.toEmail, 
			        	"Hello " + reData.name + " thanks for signing up as <b>" + 
			        	reData.role + "</b> your username is " +  reData.email + 
			        	" and password is " + reData.passwd + ", to" +
		    	  		" <a href='http://enterprisegarden.in:4000/login'>" + 
		    	  		" login click here </a>", "[SCP] Re-Invite for you " + reData.name, req.switch)
		    	}
		    	else
		    	{
		    		r.ok = false
		    	}
		    	res.json(r)


		    })
		})

		app.post("/scp/:companyName/:role/reInvite", function (req, res, next) {
			var filter = {}

			if(req.params.role != "su")
			{
				var filter = { "__db" : req.switch.db }
			}
			filter.email = req.body.email
			console.log(filter)
			register.findOne(filter, function (reErr, reData) {
				var r = { data: reData, err: reErr}
				r.ok = (reErr ? false : true)
				if(reData)
				{
					var subject = "[SCP] Re-Invite for you " + reData.name
					if(!req.body.reInvite)
					{
						subject = "[SCP] Thanks for joining with us " + reData.name
					}
				   	console.log("email this user to " + req.body.toEmail)
				   	require("../handlers/email-handler").sendEmail(req.body.toEmail, 
				       	"Hello " + reData.name + " thanks for signing up as <b>" + 
				       	reData.role + "</b> your username is " +  reData.email + 
				       	" and password is " + reData.passwd + ", to" +
				   	 	" <a href='http://enterprisegarden.in:4000/login'>" + 
				   	 	" login click here </a>", 
				   	 	subject, req.switch)
				}
				else
				{
				  	r.ok = false
				}
			   	res.json(r)

			})
		})




		//////////////////////////////////////////////////////
		//		SUPER USER
		/////////////////////////////////////////////////////
		app.get("/scp/:companyName/su/create-client", function (req, res) {

			primusAdmin.find({}, function (mongoErr, mongoData) {

	            //mongoData = sharedFunctions.getMongoData(mongoErr, mongoData)

	            //console.log(mongoErr, mongoData)

	            res.render(
	                'primus-admin', 
	                { 
	                    title : "Onboard new Client",
	                    registerData : primusAdmin.schema.paths,
	                    dataRegister : mongoData
				    }
	            )
	        })
		})

		app.post("/scp/:companyName/su/create-client", function (req, res) {
		    req.body._uname = "admin@" + req.body.domain
		    var newPrimusAdmin = new primusAdmin(req.body);
		    newPrimusAdmin.save(function (err, data) {
			    //console.log(err, data)
		        if(!err)
		        {

		        	var regObject = {
			        	name : req.body.contactPerson,
			        	email : "admin@" + req.body.domain,
			        	passwd : "welcome@123",
			        	role : "admin",
			        	__toEmail: req.body.email

			        }

			        new register(regObject).save(function (me, md) {
			        	console.log(me, md)
				        if(!me)
				        {
					        require("../handlers/email-handler").sendEmail(req.body.email, 
					        	"Hello " + regObject.name + " thanks for signing up as <b>" + 
					        	regObject.role + "</b> your username is " +  regObject.email + 
					        	" and password is " + regObject.passwd + ", to" +
				    	  		" <a href='http://enterprisegarden.in:4000/scp'>" + 
				    	  		" login click here </a>", "Thanks for signing up", req.switch)
						  	res.json({ data: data, err: err, ok: (err ? false : true)})
		      
				        }
				        else
				        {
				        	res.json({ data: md, err: me, ok: (err ? false : true)})
				        }
			        	
			        })
		    	}
		        else
		        {
		        	res.json({ data: data, err: err, ok: (err ? false : true)})
		        }

		    })
		})

		app.get("/scp/:companyName/su/users-list", function (req, res) {
		    
		      
		    register.find({}, function (gstErr, gstData) {
		        var  mongoData = sharedFunctions.getMongoData(gstErr, gstData)
		        console.log(mongoData)
		        
		        res.render(
		            'all-users', 
		            { 
		                title : "View Users",
		                entryTitle : "Users List",
		                data : mongoData.data,
		                keys : mongoData.capKeys,
		                exactKeys : mongoData.keys,
		                /*exactKeys : ['__db', 
								     'name',
								     'email',
								     'passwd',
								     'role',
								     '__toEmail',
								     '__loginCount' ]*/

		            }
		        )
		    });
		})

		////////////////////////////////////////////////
		//		ADMIN
		////////////////////////////////////////////////
		app.get("/scp/:companyName/admin/create-supplier", function (req, res, next) {
			
			var filter = { "__db" : req.switch.db }

			admin.find(filter, function (adminErr, adminData) {
				
				//mongoData = sharedFunctions.getMongoData(adminErr, adminData)
				res.render(
					'company-admin',
					{
						title : "Invite Supplier",
						adminSchema : admin.schema.paths,
						supplierData : adminData
					}

				)

			})
			
		})
		app.post("/scp/:companyName/admin/create-user/reInvite", function (req, res, next) {

			var filter = { "__db" : req.switch.db }
		    filter.email = req.body.email
		    console.log(filter)
		    register.findOne(filter, function (reErr, reData) {
		    	var r = { data: reData, err: reErr}
	    		r.ok = (reErr ? false : true)
		    	if(reData)
		    	{
		    		console.log("email this user to " + req.body.toEmail)
		    		require("../handlers/email-handler").sendEmail(req.body.toEmail, 
			        	"Hello " + reData.name + " thanks for signing up as <b>" + 
			        	reData.role + "</b> your username is " +  reData.email + 
			        	" and password is " + reData.passwd + ", to" +
		    	  		" <a href='http://enterprisegarden.in:4000/login'>" + 
		    	  		" login click here </a>", "[SCP] Re-Invite for you " + reData.name,  req.switch)
		    	}
	    		else
		    	{
		    		r.ok = false
		    	}
		    	res.json(r)
		    })

		})


		app.get("/scp/:companyName/admin/create-user", function (req, res) {

			var filter = { "__db" : req.switch.db }
			register.find(filter, function (mongoErr, mongoData) {
	            mongoData = sharedFunctions.getMongoData(mongoErr, mongoData)
	            res.render(
	                'register', 
	                { 
	                    title : "Invite people to manage SCP",
	                    desc : "You can add other users to handle activities on portal for " + app.locals.companyName,
	                    registerData : register.schema.paths,
	                    dataRegister : mongoData.data

	                }
	            )
	        })
		})

		app.get("/scp/:companyName/admin/invite-report", function (req, res) {
			var filter = { "__db" : req.switch.db }
		   	inviteReport.find({}, function (gstErr, gstData) {
		        var  mongoData = sharedFunctions.getMongoData(gstErr, gstData)
		        console.log("mongodata", mongoData.data)
		        for (var i = mongoData.data.length - 1; i >= 0; i--) {
		        	mongoData.data[i].database = req.switch.db;
		        	mongoData.data[i].__lastMod = new Date (mongoData.data[i].__lastMod);
		        	if(mongoData.data[i].status.rejected.length == 0)
		        	{
		        		mongoData.data[i].status = "Email send";
		        	}
		        	else if(mongoData.data[i].status.accepted.length > 0 && mongoData.data[i].status.rejected.length > 0){
		        		mongoData.data[i].status = "Emails are partially send";
		        	}
		        	else
		        	{
		        		mongoData.data[i].status = "Email not send";
		        	}
				}

		        
	        	supplierProfile.find(filter, function (supErr, supData) {
	        		var supplierData = sharedFunctions.getMongoData(supErr, supData)

	        		console.log(supplierData)

		        	res.render(
			            'invite-report', 
			            { 
			                title : "Invitation Report",
			                entryTitle : "Invitation Report",
			                data : mongoData.data,
			                itKey : mongoData.keys,
			                keys : mongoData.capKeys,
			                exactKeys:[
			                			"Send Time",
			                			"From Email",
			                			"To Email",
			                			"Database",
			                			"status"
			                	]
			            }
	        		)
	        	})
		    });
			
		})

		app.post("/scp/:companyName/admin/create-user", function (req, res, next) {
		    getDbSwitch(req, res, next)
		    var newRegister = new register(req.body);

		    console.log(req.body,  newRegister)

		    newRegister.save(function (err, data) {
		        console.log(err, data)

		        if(!err)
		        	{
		        		require("../handlers/email-handler").sendEmail(req.body.__toEmail, 
			        	"Hello " + req.body.name + " thanks for signing up as <b>" + 
			        	req.body.role + "</b> your username is " +  req.body.email + 
			        	" and password is " + req.body.passwd + ", to" +
		    	  		" <a href='http://enterprisegarden.in:4000/login'>" + 
		    	  		" login click here </a>", "Thanks for signing up !", req.switch)
		        	}
		        res.json({ data: data, err: err, ok: (err ? false : true)})
		    })
		})



		////////////////////////////////////////////////////////
		//		BUYER
		///////////////////////////////////////////////////////



		app.post("/scp/:companyName/admin/create-supplier", function (req, res, next) {
			
			var filter = { "__db" : req.switch.db }
		    req.body.__db = filter.__db

		    var oneSupplier = new admin(req.body);

		    oneSupplier.save(function (me, md) {
		        var supplierEmail = req.body.supplierCode +'@'+ req.cookies.email.split('@')[1]
		        var regObject = {
		        	name : req.body.name,
		        	__toEmail : req.body.email,
		        	email : supplierEmail,
		        	passwd : "welcome@123",
		        	role : "supplier",
		        	__db : req.switch.db
		        }
			    if(!me){

			        new register(regObject).save(function (me, md) {
			        	var ok  = true
			        	if(me)
			        	{
			    			console.log(JSON.stringify(me))
			        		if(me.code != 11000)
				        	{
				        		oneSupplier.remove(function (me, md) {
				        			console.log("removed ...", me)		
			        			})
			        			register.remove({email : supplierEmail}, function (me, md) {
				        			console.log("removed ...", me)		
			        			})
			        			ok = false
				        	}
				        	else
				        	{
				        		ok = true 
				        		me = null
				        	}
				        }
				        if(ok)
				        {
				        		require("../handlers/email-handler").sendEmail(req.body.email, 
					        	"Hello " + regObject.name + " thanks for signing up as <b>" + 
					        	regObject.role + "</b> your username is " +  regObject.email + 
					        	" and password is " + regObject.passwd + ", to" +
				    	  		" <a href='http://enterprisegarden.in:4000/login'>" + 
				    	  		" login click here </a>", "Thanks for signing up", req.switch)
				        	
				        }
			        	console.log(me, md)
			        	res.json({ data: md, err: me, ok: (me ? false : true)})
			        })
		        }

		        else
		        {
		        	res.json({ data: md, err: me, ok: (me ? false : true)})
		        }
		    })
		})

		app.get("/scp/:companyName/admin/create-bulk-suppliers", function(req, res){
	        res.render(
	                'file-gst', 
	                { 
	                    title : "Invite suppliers with excel file ",
	                    "btn" : "Invite all suppliers",
	                    "msg" : "We are sending invites to all given suppliers",
	                    "downloadFilePath" : "/../../assets/supplier-list.xls"
	                }
	            )
	    })

		app.post("/scp/:companyName/admin/create-bulk-suppliers", function (req, res, next) {
			
			var filter = { "__db" : req.switch.db }

		    for (var i = req.body.length - 1; i >= 0; i--) {
		    	var oneSupplierObject = req.body[i]
		    	oneSupplierObject.__db = filter.__db


		    	oneSupplierObject.__uname = oneSupplierObject.supplierCode +'@'+ req.cookies.email.split('@')[1]
		    	
		    	oneSupplierObject._role = "supplier"



			    var oneSupplier = new admin(oneSupplierObject);
			    console.log("Saving...",  oneSupplierObject)
			    oneSupplier.save(function (me, md) {
			    	console.log("save result for supplier ",md, me)
				    if(!me){
				    	oneSupplierObject = md
				        var supplierEmail = oneSupplierObject.__uname
				        
				        var regObject = {
				        	name : md.name,
				        	__toEmail : oneSupplierObject.email,
				        	email : supplierEmail,
				        	passwd : "welcome@123",
				        	role : "supplier",
				        	__db : req.switch.db
				        }
				    	console.log("Registering in logins for " , regObject)
				        new register(regObject).save(function (me, md) {
				        	var ok  = true
				        	if(me)
				        	{
				    			console.log(JSON.stringify(me))
				        		if(me.code != 11000)
					        	{
					        		newPrimusAdmin.remove(function (me, md) {
					        			console.log("removed ...", me)		
				        			})
				        			register.remove({email : supplierEmail}, function (me, md) {
					        			console.log("removed ...", me)		
				        			})
				        			ok = false
					        	}
					        	else
					        	{
					        		ok = true 
					        		me = null
					        	}
					        }
					        if(ok)
					        {
					        		//console.log("prior to send email" , regObject)
					        		require("../handlers/email-handler").sendEmail(regObject.__toEmail, 
						        	"Hello " + regObject.name + " thanks for signing up as <b>" + 
						        	regObject.role + "</b> your username is " +  regObject.email + 
						        	" and password is " + regObject.passwd + ", to" +
					    	  		" <a href='http://enterprisegarden.in:4000/login'>" + 
					    	  		" login click here </a>", "Thanks for signing up", req.switch)
					        	
					        }
				        	console.log(me, md)
				        	//res.json({ data: md, err: me, ok: (me ? false : true)})
				        })
			        }

			        else
			        {
			        }
			    })
		    }
	    	res.json({ data: "We are working on this ",  ok:  true})
		})


		app.get("/scp/:companyName/supplier/create-gst", function (req, res, next) {
			
				gst.find({}, function (gstErr, gstData) {
		            mongoData = sharedFunctions.getMongoData(gstErr, gstData)
		            res.render(
		                'order', 
		                { 
		                    title : "Manual Invoice / ASN Filing",
		                    gstSchema : gst.schema.paths,
		                    orderData : mongoData.data
		                }
		            )
		        });
			
		})

		app.get("/scp/:companyName/supplier/file-gst", function(req, res){
	        res.render(
	                'file-gst', 
	                { 
	                    title : "Upload Invoice / ASN using excel ",
	                    "downloadFilePath" : "/../../assets/gst.xlsx"
	                }
	            )
	    })

		app.post("/upload-doc", function(req, res, next){
		    getDbSwitch(req, res, next)
		    var isSaved = false;
		    for (var i = 0; i < req.body.gstArray.length; i++) {
		        var oneFile = req.body.gstArray[i]
		        oneFile.filePathAddress = "/uploads/" + oneFile.addressDocumentName + '.' + oneFile.addressDocumentType
		        oneFile.filePathGst = "/uploads/" + oneFile.gstnDocumentName + '.' + oneFile.gstnDocumentType
		        fs.writeFileSync("./public" + oneFile.filePathAddress, 
		                     decodeBase64Image(oneFile.addressData).data)

		        fs.writeFileSync("./public" + 	oneFile.filePathGst, 
		                     decodeBase64Image(oneFile.gstnData).data)

		        delete oneFile.gstnData
		        delete oneFile.addressData
		    }
		    setTimeout(function() {
		        var profile = new supplierProfile(req.body);
		        //if(!isSaved && (i == (req.body.gstArray.length - 1)))
		        profile.save(function (err, data) {    
		            console.log("db...", err, data)
		            isSaved = true
		            res.json(
		                { 
		                    data: data, 
		                    err: err, 
		                    ok: (err ? false : true)
		                }
		            )
		        })
		    }, 3000)
		})


		function decodeBase64Image(dataString) {
		    var data = dataString.split(";base64,"), response = {}
		    response.type = data[0];
		    response.data = new Buffer(data[1], 'base64');

		    return response;
		}
		app.post("/scp/:companyName/supplier/create-gst", function(req, res){
	        var gst = require("../data-models/gst")
	        var isOk = false, me = null
	        var pushed = 0
	        for (var i = req.body.length - 1; i >= 0; i--) {
	            var oneGst = req.body[i]
	            oneGst.__db = req.switch.db
	            oneGst.__uname = req.switch.__uname
	            if(oneGst.excise_invoice_date)
	            {
	                oneGst.excise_invoice_date = new Date(oneGst.excise_invoice_date)
	            }
	            new gst(oneGst).save(function(me, md){
	                console.log(me, md)
	                isOk = (me ? false : true)
	                if(!isOk)    
	                {
	                    me = me
	                    console.log("There is an err")
	                    res.send({ ok : isOk, err: me })  
	                    
	                    return false

	                }
	                pushed++;

	            })
	        
	        }
	        setTimeout(function () {
	            if(pushed == req.body.length)
	            try
	            {
	                res.send({ok : true})  
	            }
	            catch(e)
	            {
	                console.log(e)
	            }
	        }, 2000)

	    })

	    app.post("/scp/:companyName/supplier/file-gst", function(req, res){
	        var gst = require("../data-models/gst")
	        var isOk = false, me = null
	        var pushed = 0
	        for (var i = req.body.length - 1; i >= 0; i--) {
	            var oneGst = req.body[i]
	            oneGst.__db = req.switch.db
	            oneGst.__uname = req.switch.__uname
	            if(oneGst.excise_invoice_date)
	            {
	                oneGst.excise_invoice_date = new Date(oneGst.excise_invoice_date)
	            }
	            new gst(oneGst).save(function(me, md){
	                console.log(me, md)
	                isOk = (me ? false : true)
	                if(!isOk)    
	                {
	                    me = me
	                    console.log("There is an err")
	                    res.send({ ok : isOk, err: me })  
	                    
	                    return false

	                }
	                pushed++;

	            })
	        
	        }
	        setTimeout(function () {
	            if(pushed == req.body.length)
	            try
	            {
	                res.send({ok : true})  
	            }
	            catch(e)
	            {
	                console.log(e)
	            }
	        }, 2000)

	    })

	    app.get("/scp/:companyName/supplier/user", function(req, res){
	    	console.log("email.........",req.cookies.email)
	    	var supCode = ""+req.cookies.email.split('@')[0]
	    	
	    	var states = require("../config/sates");
	    	
	    	var filter = { "__db" : req.switch.db }
	    	admin.findOne({supplierCode: supCode}, function (adminErr, adminData) {
				primusAdmin.find({domain : req.switch.companyName.replace(/_/g,".")}, function (contentE, contentD) {
					console.log(adminErr,contentD)
					console.log(contentD[0])
					console.log(contentD = JSON.parse(JSON.stringify(contentD[0])))
					console.log(adminData = JSON.parse(JSON.stringify([adminData][0])))
					supplierProfile.find(filter, function(me, md){
						mongoData = sharedFunctions.getMongoData(me, md)
						res.render(
							'user',
							{
								title : "Edit Profile",
								profileSchema : contentD.format,
								dataProfile : contentD,
								adminSchema : admin.schema.paths,
								companyData : adminData,
								states : states.states,
								gsts : mongoData
							}

						)
					})
				})

			})
	        
	    })

	  /*  app.post("/scp/:companyName/supplier/user", function (req, res, next) {

			updateProfile = require("../data-models/user-profile")
		    var oneProfile = new updateProfile(req.body);
		    oneProfile.save(function (err, data) {
			    //console.log(err, data)
		        if(!err)
		        {

		        	console.log("save")
		    	}
		        else
		        {
		        	res.json({ data: data, err: err, ok: (err ? false : true)})
		        }

		    })

		})

	*/    app.put("/scp/:companyName/supplier/user", function(req, res){

		    var mc = require("../data-models/company-admin")
		    updateData(req, res, mc, "supplierCode")
		})


	    

		app.get("/scp/:companyName/buyer/view-gst", function (req, res) {
		    var filter = { "__db" : req.switch.db }
		    gst.find(filter, function (gstErr, gstData) {
		        
		        var  mongoData = sharedFunctions.getMongoData(gstErr, gstData)
		        console.log("mongoData", mongoData)
		        
		        res.render(
		            'index', 
		            { 
		                title : "View GST",
		                entryTitle : "Invoice / ASN submitted by suppliers",
		                data : mongoData.data,
		                keys : mongoData.capKeys
		            }
		        )
		    });
		})

		app.get("/scp/:companyName/buyer/view-suppliers", function (req, res) {
		    var filter = { "__db" : req.switch.db }
		    admin.find(filter, function (gstErr, gstData) {
		        var  mongoData = sharedFunctions.getMongoData(gstErr, gstData)
		        //console.log(mongoData)
		        	supplierProfile.find(filter, function (supErr, supData) {
		        		var supplierData = sharedFunctions.getMongoData(supErr, supData)

		        		console.log(supplierData)

			        	res.render(
				            'supplier-list', 
			            { 
			                title : "View Suppliers",
			                entryTitle : "Suppliers List",
			                data : mongoData.data,
			                itKey : mongoData.keys,
			                keys : mongoData.capKeys
			            }
		        		)
		        	})
		    });
		})

		app.get("/scp/:companyName/accountant/view-suppliers", function (req, res) {
		    var filter = { "__db" : req.switch.db }
		    admin.find(filter, function (gstErr, gstData) {
		        var  mongoData = sharedFunctions.getMongoData(gstErr, gstData)
		        //console.log(mongoData.__uname)
		        	supplierProfile.find(filter, function (supErr, supData) {
		        		console.log(supData)
		        		//var supplierData = sharedFunctions.getMongoData(supErr, supData)
	/*	        		for (var i = 0; i < supData.length; i++) {
		        			
		        			for (var j = 0; j < gstData.length; j++) {
		        			
				        		if(gstData[j].__uname == supData[i].uname)
				        		{
				        			logger.info("matched", gstData)
				        			if(!gstData[j].gstArray)
				        			{
				        				gstData[j].gstArray = []
				        			}
				        			//if(gstData[j] && gstData[j].gstArray)
				        			{
				        				gstData[j].bhushan = "supData[i].gstArray"
				        				logger.warn(supData[i].gstArray, "appended to array", JSON.stringify(gstData[j]))
				        			}
								}
		        			}

		        		}

	*/
		        		
			        	res.render(
				            'supplier-list', 
				            { 
				                title : "View Suppliers",
				                entryTitle : "Suppliers List",
				                data : mongoData.data,
				                itKey : mongoData.keys,
				                keys : mongoData.capKeys
				            }
	        			)

		        	})
		    });
		})


		app.get("/scp/:companyName/accountant/view-gst", function (req, res) {
		    
			var filter = { "__db" : req.switch.db }
		    
		    gst.find(filter, function (gstErr, gstData) {
		        var  mongoData = sharedFunctions.getMongoData(gstErr, gstData)
		        //console.log(mongoData)
		        
		        res.render(
		            'index', 
		            { 
		                title : "View GSTs",
		                entryTitle : "Invoice / ASN submitted",
		                data : mongoData.data,
		                keys : mongoData.capKeys
		            }
		        )
		    });
		})


		app.get("/scp/:companyName/supplier/view-gst", function (req, res) {
		    
			var filter = { "__db" : req.switch.db, "__uname" : req.cookies.email }
		    
		    gst.find(filter, function (gstErr, gstData) {
		        var  mongoData = sharedFunctions.getMongoData(gstErr, gstData)
		        console.log(mongoData)
		        
		        res.render(
		            'index', 
		            { 
		                title : "View GSTs",
		                entryTitle : "Invoice / ASN submitted",
		                data : mongoData.data,
		                keys : mongoData.capKeys
		            }
		        )
		    });
		})
		app.get("/scp/:companyName/admin/view-suppliers", function (req, res) {
			    var filter = { "__db" : req.switch.db }
			    admin.find(filter, function (gstErr, gstData) {
			        var mongoData = sharedFunctions.getMongoData(gstErr, gstData)
			        supplierProfile.find(filter, function(err,data){
			        	console.log("data....",data)
				        console.log(mongoData.keys)
		            	res.render(
				            'supplier-list', 
				            { 
				                title : "View Suppliers",
				                entryTitle : "Suppliers List",
				                data : mongoData.data,
				                itKey : mongoData.keys,
				                keys : mongoData.capKeys
				            }
		        		)
			        })
	        	})
	    
			})
		app.get("/scp/:companyName/admin/view-users", function (req, res) {
		    
		    var filter = { "__db" : req.switch.db }
		    
		    register.find(filter, function (gstErr, gstData) {
		        var  mongoData = sharedFunctions.getMongoData(gstErr, gstData)
		        console.log(mongoData)
		        
		        res.render(
		            'index', 
		            { 
		                title : "View Users",
		                entryTitle : "Users List",
		                data : mongoData.data,
		                keys : mongoData.capKeys,
		                itKeys : mongoData.keys
		            }
		        )
		    });
		})
	 	app.post("/scp/:companyName/supplier/file-gst", function(req, res, next){
	        getDbSwitch(req, res, next)
	        var gst = require("../data-models/gst")
	        var isOk = false, me = null
	        var pushed = 0
	        console.log(req.body)
	        for (var i = req.body.length - 1; i >= 0; i--) {
	            var oneGst = req.body[i]
	            oneGst.__db = req.switch.db
	            console.log(oneGst)
	            for(key in oneGst)
	            {
	                if(key.indexOf("date") != -1)
	                {
	                    oneGst[key] = new Date(oneGst[key])
	                }
	            }
	            new gst(oneGst).save(function(me, md){
	                //console.log(me, md)
	                isOk = (me ? false : true)
	                if(!isOk)    
	                {
	                    me = me
	                    console.log("There is an err")
	                    res.send({ ok : isOk, err: me })  
	                    return false

	                }
	                pushed++;

	            })
	        
	        }
	        setTimeout(function () {
	            if(pushed == req.body.length)
	            try
	            {
	                res.send({ok : true})  
	                var fs = require('fs');
	                var pdf = require('html-pdf');
	                var options = { format: 'Letter' };
	                //var options = require("../config/pdf-config");
	                var d = (req.body)

	                request.post(
	                {
	                    url: "http://localhost:4000/get-invoice", 
	                    json: d

	                }, 
	                function(err,httpResponse,body){
	                    var html = body
	                    pdf.create(html, options).toFile('./pdfs/' + Math.random() + ".pdf", function(err, res) {
	                      if (err) return console.log(err);
	                      console.log(res); // { filename: '/app/businesscard.pdf' } 
	                    });

	                });
	                    
	                
	            }
	            catch(e)
	            {
	                console.log(e)
	            }
	        }, 2000)

	    })
		app.get("/scp/:companyName/su/view-clients", function (req, res) {
		     
		    primusAdmin.find({}, function (gstErr, gstData) {
		        var  mongoData = sharedFunctions.getMongoData(gstErr, gstData)
		        console.log(mongoData)
		        
		        res.render(
		            'index', 
		            { 
		                title : "View Clients",
		                entryTitle : "Client List",
		                data : mongoData.data,
		                keys : mongoData.capKeys,
		                itKeys : mongoData.keys,
		                exactKeys : ['__db', 
								     'name',
								     'email',
								     'passwd',
								     'role',
								     '__toEmail',
								     '__loginCount' ]
		            }
		        )
		    });
		})

		app.post("/scp/login/forgot-password", function (req, res) {
			var filter = req.body
		    delete filter.__db
		    
		    register.findOne(filter, function (reErr, reData) {
		    	console.log(reErr, reData, filter)
		    	var r = { err: reErr}
	    		r.ok = (reErr ? false : true)
	    		console.log("..................................", r)
	    		
		    	if(reData)
		    	{
		    		
		    		require("../handlers/email-handler").sendEmail(reData.toEmail, 
			        	"Hello " + reData.name + ",as you are requested details for login <b></b> your username is " +  reData.email + 
			        	" and password is " + reData.passwd + ", to" +
		    	  		" <a href='http://enterprisegarden.in:4000/scp'>" + 
		    	  		" login click here </a>", "Request for password " + reData.name, req.switch)
		    	}
		    	
		    })	

		})




		function updateData(req, res, mc, key) {
			var filter = {}
			filter[key] = req.body[key]

	    	mc.update(filter, req.body, {upsert : true}, function(me, md){
		        console.log(me, JSON.stringify(md))
		        var  mongoData = sharedFunctions.getMongoData(me, md)
		        res.send({ok: me ? false : true, data: md, err: me})
		    })
		    
		}

	    function unique(array){
	        return array.filter(function(el, index, arr) {
	            return index === arr.indexOf(el);
	        });
	    }


	}