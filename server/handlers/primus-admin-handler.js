var logger    = require('../config/app-log').logger

var primusAdmin     = require('../data-models/primus-admin-data-model')
var register     = require('../data-models/register')
var syncServer = require("../server")
var sharedFunctions = require("./share-function")
module.exports = function(app){

	app.get("/primus-admin", function (req, res) {

		primusAdmin.find({}, function (mongoErr, mongoData) {

            mongoData = sharedFunctions.getMongoData(mongoErr, mongoData)

            //console.log(mongoErr, mongoData)

            res.render(
                'primus-admin', 
                { 
                    title : "Register",
                    registerData : primusAdmin.schema.paths,
                    dataRegister : mongoData.data
			    }
            )
        })
	})

	app.post("/register-admin", function (req, res) {
	    var newPrimusAdmin = new primusAdmin(req.body);

	    //console.log(req.body,  newPrimusAdmin)

	    newPrimusAdmin.save(function (err, data) {
	        //console.log(err, data)
	        var regObject = {
	        	name : req.body.contactPerson,
	        	email : req.body.email,
	        	passwd : "welcome@123",
	        	role : "admin"

	        }
	        require("../handlers/email-handler").sendEmail(req.body.email, 
	        	"Hello " + regObject.name + " thanks for signing up as <b>" + 
	        	regObject.role + "</b> your username is " +  regObject.email + 
	        	" and password is " + regObject.passwd + ", to" +
    	  		" <a href='http://enterprisegarden.in:4000/login'>" + 
    	  		" login click here </a>")
	        new register(regObject).save(function (me, md) {
	        	
	        	console.log(me, md)
	        })
	        res.json({ data: data, err: err, ok: (err ? false : true)})
	    })
	})
}