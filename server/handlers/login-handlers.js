var register 		= require("../data-models/register")
var company 		= require("../data-models/primus-admin-data-model")
module.exports = function (app) {
	
	app.get("/scp/login", function (req, res) {
		//console.log(res)
    	for(var oneCookie in req.cookies)
        {
            res.clearCookie(oneCookie);
        }
    	res.render('login', { title: 'Login', company : app.locals.companyName });
	})


	app.post("/login", function (req, res) {
	    
	    var userResponse = {role: null, token: null, ok : true, email: null, name: null}
	    
	    register.findOne({email: req.body.userName, passwd : req.body.passwd}, function(mongoErr, mongoData){
	        //console.log(mongoErr, JSON.stringify(mongoData))
	        if(null != mongoData)
	        {
	            userResponse.token = Math.random()
	            userResponse.role = mongoData.role
	            userResponse.email = mongoData.email
	            userResponse.name = mongoData.name
	  
	            for(var key in userResponse)
	            {
	                res.cookie(key, userResponse[key])
	            }
	            register.update(
	            	{email: req.body.userName, passwd : req.body.passwd}, 
	            	{$inc : {__loginCount : 1}},
	            	function(mongoErr, mongoData){
	            	
	            })
	            res.send(userResponse)
	        }
	        else
	        {
	        	userResponse.ok = false
	            res.send(userResponse)
	        }
	    })

	})
}