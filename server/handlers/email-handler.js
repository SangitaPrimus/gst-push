var nodemailer  = require('nodemailer');
var logger		= require('../config/app-log').logger
var config 		= require("../config/config") 
var fs 			= require("fs") 
var primusAdmin = require('../data-models/primus-admin-data-model')
var inviteReport = require("../data-models/invitation-report")

logger.info("Activated pool (" + config.smtpConfig.pool + 
			") for email address : " + config.smtpConfig.auth.user )

// set default options for mail, this can be overridden later 
var mailOptions = {
    from:  "Bhushan from EApps <" +config.smtpConfig.auth.user + ">", // sender address 
    to: config.configEmails.statReceiver, // list of receivers 
    bcc : config.smtpConfig.auth.user,
    subject: '[ SCP ] Thanks for signing up ', // Subject line ,
    html: "Hello thanks for signing up your username is " + 
    	  " su@primustechsys.com and password is welcome123, to" +
    	  " <a href='http://enterprisegarden.in:4000/scp'> login click here </a>"
};
var defaultMailOptions = mailOptions 
var transporter = nodemailer.createTransport(config.smtpConfig);

logger.debug("Probing transporter, is it initialized ? " + 
			(transporter != null) )

/*transporter.verify(function(error, success) {
   if (error) {
        console.log(error);
   } else {
        console.log('Server is ready to take our messages');
   }
});
*/
var sendEmail = function (to, html, subject, requestSwitch) {
	var messages = [];

	if(requestSwitch && requestSwitch.companyName && requestSwitch.companyName != "eapp_com")
	{
		primusAdmin.find({domain : requestSwitch.companyName.replace(/_/g,".")}, function (contentE, contentD) {
			console.log(contentE, contentD)
			contentD = contentD[0]
			if(!contentD)
			{
				logger.error("No config found for sending emails, failed to build")
			}
			
			contentD = JSON.parse(JSON.stringify(contentD))
			console.log("@@@@@@@@@", contentD)
			mailOptions.from = (contentD.displayName) + "<" + contentD.user + ">"
			mailOptions.bcc = contentD.user
			var config = {
				host : contentD.host,
				port : contentD.port,
				secure : contentD.secure,
				auth : {
					user : contentD.user,
					pass : contentD.pass
				}
			}
			console.log("###################################", config)
			transporter = nodemailer.createTransport(config)

			if(to)
			{
				mailOptions.to = to;
			}
			if(html)
			{
				mailOptions.html = html;
			}
			if(subject)
			{
				mailOptions.subject = subject
			}
			console.log(mailOptions)
			//for (var i = 0; i < 1; i++) {
				messages.push(mailOptions)
			//}

			//transporter.on('idle', function(){
				var msg = messages.shift()
				logger.info("## Send Email to " + msg.to)
			    // send next messages from the pending queue 
			    //while(transporter.isIdle() && messages.length){


			    	console.log(msg)
			        transporter.sendMail(msg, function(err, info){
			        	if(err){
					        return console.log(err);
					    }
					    logger.info('Message sent: ' , info);

					    var newInviteReport = new inviteReport({
					    	fromEmail : contentD.user, 
					    	toEmail: msg.to, 
					    	database:" ",
					    	status : err || info
					    })
				    	newInviteReport.save(function (err, data) {
				    		console.log("Email.............",err,data)
				    	})	
			        });
			        logger.info('Message sent: ');
			    //}
			    
			//});
		})
	}
	else
	{
		mailOptions = defaultMailOptions;
		logger.warn("Using default config for sending emails")
		transporter = nodemailer.createTransport(config.smtpConfig)
			
		if(to)
		{
			mailOptions.to = to;
		}
		if(html)
		{
			mailOptions.html = html;
		}
		if(subject)
		{
			mailOptions.subject = subject
		}
		console.log(mailOptions)
		//for (var i = 0; i < 1; i++) {
			messages.push(mailOptions)
		//}

		//transporter.on('idle', function(){
			var msg = messages.shift()
			logger.info("## Send Email to " + msg.to)
		    // send next messages from the pending queue 
		    //while(transporter.isIdle() && messages.length){


		    	console.log(msg)
		        transporter.sendMail(msg, function(err, info){
		        	if(err){
				        return console.log(err);
				    }
				    logger.info('Message sent: ' , info);

				    var newInviteReport = new inviteReport({
					    	fromEmail : msg.from, 
					    	toEmail: msg.to, 
					    	database:" ",
					    	status : err || info
					    })
				    	newInviteReport.save(function (err, data) {
				    		console.log("Email.............",err,data)
				    	})	
		        });
		        logger.info('Message sent: ');
		    //}
		    
		//});
	}

}

exports.sendEmail = sendEmail;