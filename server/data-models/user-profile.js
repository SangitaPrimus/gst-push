var mongoose = require('mongoose');

var Promise = require("bluebird");
// promise to mongoose
mongoose.Promise = Promise;
var Schema = mongoose.Schema;
// create schema less model
var supplierProfile = new Schema({

	uname :{type: String},
	gstArray : [
		{
	
			stateCode :{
				type: String
			},
			gstn: {
				type: String
			},
			address: {
				type: String
			},
			certificate:{
				type: String
			},
			addressDocumentName : {
				type: String
			},
			addressDocumentType : {
				type: String
			},
			gstnDocumentName : {
				type: String
			},
			gstnDocumentType : {
				type: String
			},
			filePathAddress :{
				type: String 
			},
			filePathGst :{
				type: String 
			} 
		}
	]
}, {strict : false})

var metaSchema = require("./plugins/meta-schema")
supplierProfile.plugin(metaSchema, {index: true})


var supplier = mongoose.model('supplier_profile', supplierProfile);

	

// make this available to our users in our Node applications
module.exports = supplier; 

