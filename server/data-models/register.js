var mongoose = require('mongoose');
var metaSchema =  require("./plugins/meta-schema")
var Promise = require("bluebird");
mongoose.Promise = Promise;
var Schema = mongoose.Schema;

var registerLoginSchema = 
	new Schema(
		{	
			name : {
				type: String, 
				validate: /[a-z]/, 
				minlength: 3, lableText:"", 
				elementType: "TEXT", 
				placeholder: "Full Name", 
				required: [true, "Please enter valid name"]
			},
			email : {
				type: String, 
				match: /\S+@\S+\.\S+/, 
				unique: true, lableText:"", 
				elementType: "EMAIL", 
				placeholder: "Email address to send invite", 
				required:[true, "Please enter valid email"]
			},
		 	passwd : {
		 		type: String, 
		 		minlength: 3, 
		 		lableText:"", 
		 		elementType: "PASSWORD", 
		 		placeholder: "Password", 
		 		required: [true, "Please enter valid password"]
		 	},
		 	role : {
		 		type: String, 
		 		enum :[ "admin", "accountant", "buyer", "supplier" ],
		 		lableText:"Select Role", 
		 		elementType: "SELECT", 
		 		placeholder: "" ,
		 		required:[true, "Please select valid role"] 
		 	},
			__db : {
				type: String
			},
			__toEmail: {
				type: String
			},
			__loginCount : {
				type: Number, default : 0
			}
		},
		{ strict: true }
	);


var metaSchema = require("./plugins/meta-schema")
registerLoginSchema.plugin(metaSchema, {index: true})

var register = mongoose.model('registerLogin', registerLoginSchema);
module.exports = register; 