var mongoose = require('mongoose');
var Promise = require("bluebird");
// promise to mongoose
mongoose.Promise = Promise;
var Schema = mongoose.Schema;
// create schema less model
var companyAdmin = new Schema({

	supplierCode : {
		type: String, 
		lableText:"", 
		elementType: "TEXT", 
		placeholder: "Supplier Code", 
		required: [true, "Please enter valid supplier code"]
	},
	name : {
		type: String, 
		validate: /[a-z]/, 
		minlength: 3, 
		lableText:"", 
		elementType: "TEXT",
		placeholder: "Supplier Name", 
		required:[true, "Please enter valid supplier name"]
	},
 	email : {
 		type: String, 
 		match: /\S+@\S+\.\S+/, 
 		lableText:"", 
 		elementType: "EMAIL", 
 		placeholder: "Email address to send invite", 
 		required:[true, "Please enter valid email"]
 	},
	/*phone: {
		type: String, 
		lableText:"Phone No", 
		elementType: "TEXT", 
		placeholder: "Phone Number",
		minlength:10,
		maxlength:10
		required: [true, "Please enter valid phone number"]
	},
 	pan : {
 		type: String, 
 		match:/[A-Za-z]{5}[0-9]{4}[A-z]{1}/, 
 		lableText:"", 
 		elementType: "TEXT", 
 		placeholder: "PAN Number"
 	},*/
	_role : {
		type: String, 
		default: "supplier"
	},
	_password : {
		type: String, 
		default: "welcome@123"
	},
	__uname : {
		type: String, 
		unique: true
	}
}, {strict : false})

var metaSchema = require("./plugins/meta-schema")
companyAdmin.plugin(metaSchema, {index: true})

var admin = mongoose.model('supplier', companyAdmin);
module.exports = admin; 

