var mongoose = require('mongoose');

var Promise = require("bluebird");
// promise to mongoose
mongoose.Promise = Promise;
var Schema = mongoose.Schema;
// create schema less model
var gstSchema = new Schema(
	{ 
		gstn_no_of_supplier: { 
	   		type: String,
	     	required: [ true, 'Please enter valid gstn no of supplier' ],
	     	elementType: 'TEXT',
	     	elementText: 'Gstn No Of Supplier',
	     	placeholder: 'Gstn No Of Supplier' 
	    },
	  	supplier_name: { 
	   		type: String,
	     	required: [ true, 'Please enter valid supplier name' ],
	        elementType: 'TEXT',
	        elementText: 'Supplier Name',
	        placeholder: 'Supplier Name' 
	    },
	    invoice_no: {    
	   	    type: String,
	        required: [ true, 'Please enter valid invoice no' ],
	        elementType: 'TEXT',
	        elementText: 'Invoice No',
	        placeholder: 'Invoice No' ,

	    },
	  	invoice_date: 
	   	{ 
	   	    type: String,
	        required: [ true, 'Please enter valid invoice date' ],
	        elementType: 'TEXT',
	        elementText: 'Invoice Date',
	        placeholder: 'Invoice Date' },
	  	gstn_no_buyer__bill_to: {    
	  		type: String,
	        required: [ true, 'Please enter valid gstn no buyer  bill to' ],
	        elementType: 'TEXT',
	        elementText: 'Gstn No Buyer  Bill To',
	        placeholder: 'Gstn No Buyer  Bill To' },
	  	buyer_name__bill_to: { 
		    type: String,
		    required: [ true, 'Please enter valid buyer name  bill to' ],
		    elementType: 'TEXT',
		    elementText: 'Buyer Name  Bill To',
		    placeholder: 'Buyer Name  Bill To' 
		},
	  	buyer_state__bill_to: { 
		    type: String,
		    required: [ true, 'Please enter valid buyer state  bill to' ],
		    elementType: 'TEXT',
		    elementText: 'Buyer State  Bill To',
		    placeholder: 'Buyer State  Bill To' 
		},
	  	buyer_state_code__bill_to: { 
		    type: String,
		    required: [ true, 'Please enter valid buyer state code  bill to' ],
		    elementType: 'TEXT',
		    elementText: 'Buyer State Code  Bill To',
		    placeholder: 'Buyer State Code  Bill To' 
		},
	  	gstn_no_buyer__ship_to: { 
		    type: String,
		    required: [ true, 'Please enter valid gstn no buyer  ship to' ],
		    elementType: 'TEXT',
		    elementText: 'Gstn No Buyer  Ship To',
		    placeholder: 'Gstn No Buyer  Ship To' 
		},
	  	buyer_name__ship_to: { 
		    type: String,
		    required: [ true, 'Please enter valid buyer name  ship to' ],
		    elementType: 'TEXT',
		    elementText: 'Buyer Name  Ship To',
		    placeholder: 'Buyer Name  Ship To' 
		},
		buyer_state__ship_to: { 
		    type: String,
		    required: [ true, 'Please enter valid buyer state  ship to' ],
			elementType: 'TEXT',
		    elementText: 'Buyer State  Ship To',
			placeholder: 'Buyer State  Ship To' 
		},
	  	buyer_state_code__ship_to: 
	   { 
	    type: String,
	    required: [ true, 'Please enter valid buyer state code  ship to' ],
	    elementType: 'TEXT',
	    elementText: 'Buyer State Code  Ship To',
	    placeholder: 'Buyer State Code  Ship To' 
		},
		description_of_goods: { 
		    type: String,
		    required: [ true, 'Please enter valid description of goods' ],
		    elementType: 'TEXT',
		    elementText: 'Description Of Goods',
		    placeholder: 'Description Of Goods' 
		},
		hsn_code: { 
		    type: String,
		    required: [ true, 'Please enter valid hsn code' ],
		    elementType: 'TEXT',
		    elementText: 'Hsn Code',
		    placeholder: 'Hsn Code' },
		quantity: { 
		     type: String,
		     required: [ true, 'Please enter valid quantity' ],
		     elementType: 'NUMBER',
		     elementText: 'Quantity',
		     placeholder: 'Quantity' 
		},
		unit: { 
		    type: String,
		    required: [ true, 'Please enter valid unit' ],
		    elementType: 'TEXT',
		    elementText: 'Unit',
		    placeholder: 'Unit' 
		},
		rate: { 
		     type: String,
		     required: [ true, 'Please enter valid rate' ],
		     elementType: 'NUMBER',
		     elementText: 'Rate',
		     placeholder: 'Rate' 
		},
		total: { 
		     type: String,
		     required: [ true, 'Please enter valid total' ],
		     elementType: 'TEXT',
		     elementText: 'Total',
		     placeholder: 'Total' 

		},
		discount: { 
		     type: String,
		     required: [ true, 'Please enter valid discount' ],
		     elementType: 'TEXT',
		     elementText: 'Discount',
		     placeholder: 'Discount' 
		},
		taxable_value: { 
		     type: String,
		     required: [ true, 'Please enter valid taxable value' ],
		     elementType: 'NUMBER',
		     elementText: 'Taxable Value',
		     placeholder: 'Taxable Value'
		},
		cgst_percent: { 
		     type: String,
		     required: [ true, 'Please enter valid cgst percent' ],
		     elementType: 'NUMBER',
		     elementText: 'Cgst Percent',
		     placeholder: 'Cgst Percent',
		     match:/[0-9]*$/ 
		 },
		cgst_amount: { 
		     type: String,
		     required: [ true, 'Please enter valid cgst amount' ],
		     elementType: 'NUMBER',
		     elementText: 'Cgst Amount',
		     placeholder: 'Cgst Amount' ,
		     match:/[0-9]*$/
		 },
		sgst_percent: { 
		     type: String,
		     required: [ true, 'Please enter valid sgst percent' ],
		     elementType: 'NUMBER',
		     elementText: 'Sgst Percent',
		     placeholder: 'Sgst Percent',
		      match:/[0-9]*$/ 
		 },
		sgst_amount: { 
		     type: String,
		     required: [ true, 'Please enter valid sgst amount' ],
		     elementType: 'NUMBER',
		     elementText: 'Sgst Amount',
		     placeholder: 'Sgst Amount',
		    match:/[0-9]*$/
		},
		igst_percent: 
		   { 
		     type: String,
		     required: [ true, 'Please enter valid igst percent' ],
		     elementType: 'NUMBER',
		     elementText: 'Igst Percent',
		     placeholder: 'Igst Percent',
		     match:/[0-9]*$/ 
		},
		igst_amount: 
		   { 
		     type: String,
		     required: [ true, 'Please enter valid igst amount' ],
		     elementType: 'NUMBER',
		     elementText: 'Igst Amount',
		     placeholder: 'Igst Amount' ,
		     match:/[0-9]*$/
		},
		item_value: 
		   { 
		     type: String,
		     required: [ true, 'Please enter valid total invoice value' ],
		     elementType: 'NUMBER',
		     elementText: 'Item Value',
		     placeholder: 'Item Value' ,
		     match:/[0-9]*$/
		},
		__invoiceValue : {type: Number},
	   __uname : {type:String}
 	}, 
	{ strict: false }
);

var metaSchema = require("./plugins/meta-schema")
gstSchema.plugin(metaSchema, {index: true})

var gst = mongoose.model('gst', gstSchema);


// make this available to our users in our Node applications
module.exports = gst; 

