var mongoose = require('mongoose');

var Promise = require("bluebird");
// promise to mongoose
mongoose.Promise = Promise;
var Schema = mongoose.Schema;
// create schema less model
var machineSchema = new Schema({/* mention schema here */}, { strict: false });
var machine = mongoose.model('machine', machineSchema);


// make this available to our users in our Node applications
module.exports = machine; 

