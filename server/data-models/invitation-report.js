var mongoose = require('mongoose');
var metaSchema =  require("./plugins/meta-schema")
var Promise = require("bluebird");
mongoose.Promise = Promise;
var Schema = mongoose.Schema;

var invitationReportSchema = 
	new Schema(
		{	
			fromEmail : {
				type: String, 
			},
			database : {
				type: String,
			},
			__db : {
				type: String
			},
			toEmail: {
				type: String
			},
			sentTime:
			{
				type: String
			}
		},
		{ strict: false }
	);


var metaSchema = require("./plugins/meta-schema")
invitationReportSchema.plugin(metaSchema, {index: true})

var register = mongoose.model('invitationReport', invitationReportSchema);
module.exports = register; 