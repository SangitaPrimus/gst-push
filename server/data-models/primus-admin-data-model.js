var mongoose = require('mongoose');

var Promise = require("bluebird");
// promise to mongoose
mongoose.Promise = Promise;
var Schema = mongoose.Schema;
// create schema less model
var primusAdmin = new Schema({

	_uname : {
		type: String, 
		unique: true
	},
	company : {
		type: String, 
		validate: /[a-z]/, 
		minlength: 3, 
		lableText:"", 
		elementType: "TEXT", 
		placeholder: "Company", 
		required:[true, "Please enter valid company name"]
	},
 	application : {
 		type: String, 
 		enum :[ "SCP"],
 		lableText:"Select application", 
 		elementType: "SELECT", 
 		placeholder: "" ,
 		required:[true, "Please select valid application"]
 	},
 	gstn : {
 		type: String, 
 		lableText:"", 
 		elementType: "TEXT", 
 		placeholder: "GST Number"
 	},
 	pan : {
 		type: String, 
 		match:/[A-Za-z]{5}[0-9]{4}[A-z]{1}/ ,
 		lableText:"", 
 		elementType: "TEXT", 
 		placeholder: "PAN Number"
 	},
 	email : {
 		type: String, 
 		match: /\S+@\S+\.\S+/,
 		lableText:"", 
 		elementType: "EMAIL", 
 		placeholder: "Your Email", 
 		required:[true, "Please enter valid email"]
 	},
 	domain : {
 		type: String, 
 		unique:true, 
 		lableText:"", 
 		elementType: "TEXT", 
 		placeholder: "Domain Name", 
 		required:[true, "Please enter validate domain name"]
 	},
 	address : {
 		type: String, 
 		minlength: 3 ,
 		lableText: "", 
 		elementType: "TEXT", 
 		placeholder: "Address", 
 		required:[true, "Please enter valid address"]
 	},
 	contactPerson : {
 		type: String, 
 		validate: /[a-z]/, 
 		minlength: 3, 
 		lableText:"", 
 		elementType: "TEXT", 
 		placeholder: "Contact Person Name", 
 		required: [true, "Please enter valid name"]
 	},
	phone: {
		type: String, 
		lableText:"Phone No", 
		elementType: "TEXT", 
		placeholder: "Phone Number",
		minlength:10,
		maxlength:10,
		required: [true, "Please enter valid phone number"]
	},
	_role : {
		type: String, 
		default: "admin"
	},
	_password : {
		type: String, 
		default: "welcome@123"
	},
	iconUrl : {
		lableText:"Company Logo URL", 
		elementType: "TEXT", 
		placeholder: "Company Logo URL (100*100 px)",
		type : String
	}
}, {strict : false})
var admin = mongoose.model('primus_admin', primusAdmin);

	

// make this available to our users in our Node applications
module.exports = admin; 

