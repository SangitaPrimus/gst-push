// lastMod.js
module.exports = exports = function lastModifiedPlugin (schema, options) {
  	schema.add({ __lastMod: Date, __db: String,  })
  	
  	schema.pre('save', function(next,req, callback){
    	this.__lastMod = new Date
 		if(!this.__db)
 		{
 			this.__db = "gst"
 		}
 	   	next()
  	})
  
  	if (options && options.index) {
    	schema.path('__lastMod').index(options.index)
  	}
}