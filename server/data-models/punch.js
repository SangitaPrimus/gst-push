var mongoose = require('mongoose');

var Promise = require("bluebird");
// promise to mongoose
mongoose.Promise = Promise;
var Schema = mongoose.Schema;
// create schema less model
var punchSchema = new Schema(
	{
	    supplier: {
	        type: String,
	        required: [
	            true,
	            "Please TEXT valid order number"
	        ],
	        elementType: "TEXT",
	        elementText: "Supplier Code",
	        placeholder: ""
	    },
	    supplier_name: {
	        type: String,
	        required: [
	            true,
	            "Please enter valid supplier name"
	        ],
	        elementType: "TEXT",
	        elementText: "Supplier Name",
	        placeholder: ""
	    },
	    supplier_gstn_number: {
	        type: String,
	        required: [
	            true,
	            "Please enter valid GSTN"
	        ],
	        elementType: "TEXT",
	        elementText: "Enter GSTN",
	        placeholder: "",
	    },
	    customer: {
	        type: String,
	        required: [
	            true,
	            "Please enter valid customer code"
	        ], 
	        elementType: "TEXT",
	        elementText: "Customer Code",
	        placeholder: ""
	    },
	    customer_name: {
	        type: String,
	        required: [
	            true,
	            "Please enter valid customer name"
	        ],
	        elementType: "TEXT",
	        elementText: "Customer name",
	        placeholder: ""
	    },
	    excise_invoice_no: {
	        type: String,
	        required: [
	            true,
	            "Please enter valid excise invoice number"
	        ],
	        elementType: "TEXT",
	        elementText: "Excise invoice number",
	        placeholder: "Excise invoice number",
	    },

	    excise_invoice_date: {
	        type: String,
	        required: [
	            true,
	            "Please TEXT valid machine name"
	        ],
	        elementType: "TEXT",
	        elementText: "Machine Name",
	        placeholder: ""
	    },
	    material: {
	        type: Number,
	        required: [
	            true,
	            "Please enter valid quantity"
	        ],
	        elementType: "NUMBER",
	        elementText: "Quantity",
	        placeholder: "",
	        match:/[0-9]*$/
	    },
	    description: {
	        type: String,
	        required: [
	            true,
	            "Please TEXT valid UOM"
	        ],
	        elementType: "TEXT",
	        elementText: "UOM",
	        placeholder: ""
	    },
	    quantity:{
	    	type: Boolean,
	    	elementType: "CHECKBOX",
	    	elementText: "Down Time",
	    	placeholder: ""
	    },
	    unit: {
	    	type: Date,
	    	elementType: "TEXT",
	    	elementText: "Down Time From",
	    	placeholder: ""

	    },
	    base_price: {
	    	type: Date,
	    	elementType: "TEXT",
	    	elementText: "Down Time To",
	    	placeholder: ""

	    },

	    total_value: {
	    	type: String,
	    	required: [
	            true,
	            "Please TEXT valid down time reasons"
	        ],
	        elementType: "TEXT",
	        elementText: "Down Time Reasons",
	        placeholder: ""
	    },
	    tax: {
	    	type: Date,
	    	elementType: "TEXT",
	    	elementText: "Down Time To",
	    	placeholder: ""

	    },
	    total_value: {
	    	type: Date,
	    	elementType: "TEXT",
	    	elementText: "Down Time To",
	    	placeholder: ""

	    }

	}, 
	{ strict: false }
);

var punch = mongoose.model('punch', punchSchema);


// make this available to our users in our Node applications
module.exports = punch; 

