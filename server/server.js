//'use strict';

var logger    = require('./config/app-log').logger

logger.info("\n\n\n################# New server initialization is in progress " + 
                  "#################")

var Promise = global.Promise || require('promise');

var express         = require('express'),
    exphbs          = require('./config/lib/'), // "express-handlebars"
    helpers         = require('./config/lib/helpers');
var helpers         = require('handlebars-helpers')();

var bodyParser      = require('body-parser');
var cookieParser    = require('cookie-parser');

var jwt             = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config          = require('./config/config'); // get our config file
var mongoose        = require('mongoose');
var port            = process.env.PORT || 4000; // used to create, sign, and verify tokens
var app             = express();
var ipfilter        = require('express-ipfilter') // https://www.npmjs.com/package/express-ipfilter
var url             = require('url');
var access          = require('context-access');
var accessLoader    = require("./config/access")
var sharedFunctions = require("./handlers/share-function")
var gst             = require('./data-models/gst')
var supplierProfile = require("./data-models/user-profile")
var companies       = require("./data-models/primus-admin-data-model")
//importing data models

logger.debug("Imported all required libraries, looks like everything will work fine")

//end importing data 

mongoose.set("debug", "true")

// Create `ExpressHandlebars` instance with a default layout.
var hbs = exphbs.create({
    defaultLayout: 'main',
    helpers      : helpers,

    partialsDir: [
        'shared/templates/',
        'views/partials/'
    ]
    // Uses multiple partials dirs, templates in "shared/templates/" are shared
    // with the client-side of the app (see below).
});


// Register `hbs` as our view engine using its bound `engine()` function.
mongoose.connect(config.mongodb.database); // connect to database


app.engine('handlebars', hbs.engine);

app.set('superSecret', config.mongodb.secret); // secret variable

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(cookieParser())
// use morgan to log requests to the console

app.set('view engine','handlebars');

//restrict IPS
 
// Whitelist the following IPs 
var ips = ['127.0.0.1', '192.168.0.48', '192.168.0.52'];
//Using IP ranges:
//var ips = [['127.0.0.1','127.0.0.10']];

//Using CIDR subnet masks for ranges:
//var ips = ['127.0.0.1/24'];

logger.debug(ips + " are allowed access")

// set ips mode to restrict or deny, 
/*app.use(
    ipfilter(
        ips, 
        {
            mode: 'allow', 
            allowForward: true ,
            errorCode: 401,
            log: false,
            errorMessage: "<h1>It's not fair,</h1><h2>You are not allowed to access" + 
                          " machine !</h2> Take permissions first !"
        }
    )
);*/

//restrict endpoint level accesses
// Allow users with manager or admin role to POST to /users 

// Route middleware 
accessLoader.loadUrlAccess(access)

logger.debug("added access restrictions on URLs, depending on access specified")
var reqCount = 0;
app.use(function(req, res, next) {
    //logger.log(req, res, next)
    next();
});




var authorize = function(req, res, next) {
    next()
        
    /*var context = {
        role: req.cookies.role,   // admin 
        path: req.path,           // /users 
        method: req.method        // POST 
    };
    var auth = {
        inTime : new Date(),
        isAuth : false
    }
    res.authStatus = auth

    var skippablePaths = ["/login", "/register"]
    if(-1 != skippablePaths.indexOf(req.path))
    {
        next()
    }
    else
    {
        if(!context.role)
        {
            if(context.path != "/login")
            {
                res.redirect("/login")
            }
            else
            {
                next()
            }
            return false
        }

        //console.log(JSON.stringify(access.contexts))
        
    }
    if(!context.role || context.role == undefined)
    {
        context.role = "guest"
    }
    if (access.assert(context)) {
         next();
    }
    else {
       //return next()
       logger.warn("URL " + req.method + req.url + 
                   " is not accessible or available for " + 
                   context.role + ", sending err page")
       res.render('err', {
           errorTitle: "Yes it's not accessible" ,
           errorMessage: "Access is not permitted to you for resource " + 
                           req.path + "and with role " + context.role,
           goBackToUrl: (req.cookies.token ? "/" : "/login")
       });
    }
    */
};
 

// =======================
// routes ================
// =======================
var cacheTime = 86400000*7;     // 7 days
var apiRoutes = express.Router(); 
app.use(express.static('public/', {maxAge: cacheTime}));

app.all('/*', authorize, function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    logger.info( "IP : ", req.ip, 
                 " | Path : ", req.path, 
                 " | Type : ", req.method, 
                 " | Cookies : ",req.cookies, 
                 " | Body : ",req.body)
    var companyName = 
        (req.cookies.email ? 
         req.cookies.email.split("@")[1] : null)
    if(companyName)
    {
        
        app.locals.companyName = companyName
        app.locals.userRole = req.cookies.role
        app.locals.companyString = companyName.toLowerCase().replace(/\./g,"_")
        companies.findOne({domain : companyName.toLowerCase()}, function (me, md) {
            if(md)
            {
                app.locals.companyName = md.company
                app.locals.companyName = 
                    sharedFunctions.getCamelCase(app.locals.companyName)
                app.locals.companyDetails = md
            }
            else
            {
                app.locals.companyName = "Enterprise Garden"
            }
            next();
        })
    }
    else
    {
        app.locals.companyName = "Enterprise Garden"
        next();
    }
    
});

require("./handlers/order-handler")(app)
require("./handlers/register")(app)
require("./handlers/login-handlers")(app)
require("./handlers/routing-handler")(app)
require("./handlers/primus-admin-handler")(app)
app.get("/", function (req, res) {
    console.log("redirecting user to /scp")
    res.redirect("/scp")
})
app.get("/login", function (req, res) {
    console.log("redirecting user to /scp/login")
    res.redirect("/scp/login")
})

//////////////////////////////////
//      allow Cross domain     //
////////////////////////////////

app.locals.companyName = "Enterprise Garden"
app.locals.appName = "Supplier Collaboration Portal"
app.locals.skipKeys = ["__v", "_id", "__db"]
app.locals.msgLine1 = "Enterprise Products"
app.locals.msgLine2 = "FOR YOUR BUSINESS NEEDS"
app.locals.msgFooter = "Collaborator makes everything seamless"
app.locals.productCompany = "PRIMUS TechSystems Pvt. Ltd."


logger.info("authorize will work on every incoming request, but all " + 
            "origins are currently accepted ")

logger.debug("Loaded all routing handlers, processors are interlinked with them")
//end importing handlers

app.use(apiRoutes);

app.listen(port, function () {

    var mongooseState = {0 : "disconnected", 1 : "connected", 2 : "connecting", 3 : "disconnecting"}
    logger.debug("mongoose connection status : " + mongooseState[mongoose.connection.readyState]);
    
    logger.info("Server started at " + port + 
                " all incoming requests now can be served" + 
                "\n\n\n###########################################")
});
