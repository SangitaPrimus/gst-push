var mongoose = require('mongoose')

var typeMappings  =
{"String":String, 
 "Number":Number,
 "Boolean":Boolean,
 "ObjectId":mongoose.Schema.ObjectId,
  //....etc
}

function makeSchema(jsonSchema){
  var outputSchemaDef = {}
  for(fieldName in jsonSchema.data){
    var fieldType = jsonSchema.data[fieldName]
    if(typeMappings[fieldType]){
      outputSchemaDef[fieldName] = typeMappings[fieldType]
    }else{
      console.error("invalid type specified:", fieldType)
    }
  }
  return new mongoose.Schema(outputSchemaDef)
}


console.log(makeSchema({"supplier":708590,"supplier_name":"Mahalaxmi Engineers","supplier_gstn_number":"CDNPP8298PMH","customer":1000,"customer_name":"Hirschvogel Components India","excise_invoice_no":1,"excise_invoice_date":"15.10.2016","material":100001,"description":"Nut 12X10","quantity":100,"unit":"EA","base_price":10,"total_value":1180,"tax":180}))